import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';

const BASE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
];

const PRIME_MODULES = [
  TableModule,
  DropdownModule,
  CalendarModule,
  CardModule,
  ButtonModule,
  InputTextModule,
];

const COMPONENTS = [];

const ENTRY_COMPONENTS = [];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    ...BASE_MODULES,
    ...PRIME_MODULES,
  ],
  exports: [
    ...BASE_MODULES,
    ...PRIME_MODULES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class ThemeModule { }
