import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'table',
        component: TableComponent,
      },
      {
        path: '**',
        redirectTo: 'table',
      },
    ],
  },
];

export const PagesRoutingComponents = [
  PagesComponent,
  TableComponent,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule { }
