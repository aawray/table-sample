import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/api';
import { concat, empty, EMPTY, from, Observable, of } from 'rxjs';
import {
  debounceTime, filter, finalize, flatMap,
  map, skip, take, takeWhile, tap, toArray,
} from 'rxjs/operators';
import { Users } from 'src/app/@core/api/api.module';
import { IUser } from 'src/app/@core/api/v1/users';
import { DataService } from 'src/app/@core/data/data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, OnDestroy {

  users: Users.IUser[];
  totalRecords: number;
  cols: any[];
  loading: boolean;

  filters = this.fb.group({
    lastname: [],
    phone: [],
    city: [],
    dob: [],
  });

  @ViewChild('usersTable') usersTable;

  private alive = true;

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.loading = true;
    this.filters.valueChanges
      .pipe(
        tap(() => this.loading = true),
        debounceTime(500),
      )
      .subscribe((filters) => {
        if (!this.usersTable) { return; }
        Object.keys(filters).forEach((key) => {
          if (['lastname', 'phone', 'city'].indexOf(key) > -1) {
            this.usersTable.filter(filters[key], key, 'like');
          } else if (key === 'dob') {
            this.usersTable.filter(filters.dob, 'dob', 'date-between');
          }
        });
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  loadUsers(event: LazyLoadEvent) {
    this.loading = true;
    this.dataService.users().pipe(
      takeWhile(() => this.alive),
      map((data) => data.results),
      flatMap((users) => this.filterize(users, event.filters)),
      finalize(() => this.loading = false),
    )
      .subscribe((users) => {
        this.totalRecords = users.length;
        from(users)
          .pipe(
            skip(event.first),
            take(event.rows),
            toArray<IUser>(),
          )
          .subscribe(
            (proceedUsers) => {
              this.users = proceedUsers;
            },
          );
      });
  }

  clearFilters() {
    this.filters.reset();
  }

  filterize(users: IUser[], filters): Observable<Users.IUser[]> {
    return from(users).pipe(
      filter((user: IUser) => {
        let passItem = true;

        for (const field of Object.keys(filters)) {
          if (!passItem) { return false; }
          const fieldFilter = filters[field];
          let userValue = user[field];
          // NOTE: Возможно вынести в отдельный класс
          switch (field) {
            case 'lastname':
              userValue = user.name.last;
              break;
            case 'city':
              userValue = user.location.city;
              break;
            case 'phone':
              userValue = userValue.replace(/[^0-9]*/, '');
              fieldFilter.value = fieldFilter.value.replace(/[^0-9]*/, '');
              break;
            case 'dob':
              userValue = Date.parse(userValue);
              break;
            default:
              break;
          }
          // NOTE: Возможно вынести в отдельный класс
          switch (fieldFilter.matchMode) {
            case 'like':
              passItem = passItem && userValue.includes(fieldFilter.value);
              break;
            case 'date-between':
              const bounds = fieldFilter.value || [];
              const leftBound = bounds[0];
              const rightBound = bounds[1];
              passItem = passItem && (!leftBound || userValue >= leftBound) && (!rightBound || userValue <= rightBound);
              break;
            default:
              console.warn(`Unsupported filter mode ${fieldFilter.matchMode}`);
              break;
          }
        }

        return passItem;
      }),
      toArray<IUser>(),
    );
  }

}
