import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
})
export class PagesComponent implements OnInit, OnDestroy {

  private alive = true;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
