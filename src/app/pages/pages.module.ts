import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ThemeModule } from '../@theme/theme.module';
import { PagesRoutingComponents, PagesRoutingModule } from './pages-routing.module';

@NgModule({
  declarations: [
    ...PagesRoutingComponents,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    ThemeModule,
  ],
})
export class PagesModule { }
