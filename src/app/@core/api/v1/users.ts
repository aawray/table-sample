export interface IName {
  title: string;
  first: string;
  last: string;
}

export interface ILocation {
  street: string;
  city: string;
  state: string;
  postcode: number | string;
}

export interface ILogin {
  username: string;
  password: string;
  salt: string;
  md5: string;
  sha1: string;
  sha256: string;
}

export interface IId {
  name: string;
  value: string;
}

export interface IPicture {
  large: string;
  medium: string;
  thumbnail: string;
}

export interface IUser {
  gender: string;
  name: IName;
  location: ILocation;
  email: string;
  login: ILogin;
  dob: string;
  registered: string;
  phone: string;
  cell: string;
  id: IId;
  picture: IPicture;
  nat: string;
}

export interface IInfo {
  seed: string;
  results: number;
  page: number;
  version: string;
}

export interface IResponse {
  results: IUser[];
  info: IInfo;
}
