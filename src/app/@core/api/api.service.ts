import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment as $env } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  private apiUrl = $env.apiUrl;

  constructor(public http: HttpClient) { }

  get<T>(urlParts: string[] | string): Observable<T> {
    const url = [this.apiUrl].concat(urlParts).join('/');
    return (this.http.get(url, {}) as unknown) as Observable<T>;
  }
}
