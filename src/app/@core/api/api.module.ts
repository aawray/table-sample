import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import * as Users from './v1/users';
export { Users };

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [],
  declarations: [],
})
export class ApiModule { }
