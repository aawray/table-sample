import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Users } from '../api/api.module';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root',
})
export class DataService {

  constructor(
    private apiService: ApiService,
  ) { }

  users(): Observable<Users.IResponse> {
    return this.apiService.get('users.json');
  }
}
